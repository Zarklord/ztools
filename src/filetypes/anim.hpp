/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "../common.hpp"
#include <map>

class scml;
class parsed_command_line;

class anim {
	friend class scml;
	public:
	anim(const fs::path &path, parsed_command_line * cmd);
	anim(const scml scmlfile, parsed_command_line * cmd);
	void write(const fs::path &path);
	void swapsymbol(const std::string & oldsymbol, const std::string & newsymbol);
	std::list<std::string> getsymbols();
	private:

	std::string getrealstring(uint32_t hash);
	std::string getrealstring(std::string name);

	enum facing : uint8_t {
		FACING_RIGHT = 1 << 0,
		FACING_UP = 1 << 1,
		FACING_LEFT = 1 << 2,
		FACING_DOWN = 1 << 3,
		FACING_UPRIGHT = 1 << 4,
		FACING_UPLEFT = 1 << 5,
		FACING_DOWNRIGHT = 1 << 6,
		FACING_DOWNLEFT = 1 << 7,
		FACING_SIDE = FACING_LEFT | FACING_RIGHT,
		FACING_UPSIDE = FACING_UPLEFT | FACING_UPRIGHT,
		FACING_DOWNSIDE = FACING_DOWNLEFT | FACING_DOWNRIGHT,
		FACING_45S = FACING_UPLEFT | FACING_UPRIGHT | FACING_DOWNLEFT | FACING_DOWNRIGHT,
		FACING_90S = FACING_UP | FACING_DOWN | FACING_LEFT | FACING_RIGHT,
		FACING_ANY = FACING_45S | FACING_90S
	};

	struct element {
		uint32_t hash;
		uint32_t buildframe;
		uint32_t layernamehash;
		float ma;
		float mb;
		float mc;
		float md;
		float mtx;
		float mty;
		float z;
	};
	struct frame {
		float x;
		float y;
		float w;
		float h;
		uint32_t numevents;
		//std::vector<uint32_t> events;
		uint32_t numelements;
		std::vector<element> elements;
	};
	struct animation {
		std::string name;
		facing direction;
		uint32_t bankhash;
		float framerate;
		uint32_t framecount;
		std::vector<frame> frames;
	};

	parsed_command_line * cmd;

	uint32_t version{4};
	uint32_t numelements;
	uint32_t numframes;
	uint32_t numevents;
	uint32_t numanims;

	std::vector<animation> animations{};

	uint32_t hashcount;

	std::vector<uint32_t> hashlist;

	//hash, string
	std::map<uint32_t, std::string> symbollist;
};