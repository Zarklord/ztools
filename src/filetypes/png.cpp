/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "png.hpp"
#include <iostream>

#include "../cmdlinehelper.hpp"
#include "tex.hpp"

png::png(const fs::path& path, parsed_command_line * cmd) : cmd(cmd) {
	if (cmd->verbosity >= 1) std::cout << "Reading image file: " << path << std::endl;
	try {
		image.read(path.string());
		if (cmd->verbosity >= 1) std::cout << "Finished reading image file: " << path << std::endl;
	} catch (Magick::Exception e) {
		if (cmd->verbosity >= 1) std::cout << "Failed to read image file: " << path << std::endl;
	}
}

png::png(const uint16_t width, const uint16_t height, const Magick::DrawableList& drawcommands, parsed_command_line * cmd) : cmd(cmd) {
	image = Magick::Image{ Magick::Geometry{ width, height }, Magick::Color(0, 0, 0, 0) };
	image.draw(drawcommands);
}

png::png(const tex &texture, parsed_command_line * cmd) : cmd(cmd) {
	const tex::mipmap &mip = texture.mipmaps[0];
	image.read(mip.data, Magick::Geometry(mip.width, mip.height), 8, texture.datafmt);
	image.flip();
}

void png::write(const fs::path& path) {
	if (cmd->verbosity >= 1) std::cout << "Writing image file: " << path << std::endl;
	image.magick("png");
	image.defineValue("png", "color-type", "6");
	try {
		image.write(path.string());
		if (cmd->verbosity >= 1) std::cout << "Finished writing image file: " << path << std::endl;
	}
	catch (Magick::Exception e) {
		if (cmd->verbosity >= 1) std::cout << "Failed to write image file: " << path << std::endl;
	}
}

void png::write(const fs::path& path, const Magick::Geometry& crop) {
	if (cmd->verbosity >= 1) std::cout << "Writing image file: " << path << std::endl;
	Magick::Image crop_image = image;
	crop_image.crop(crop);
	crop_image.magick("png");
	crop_image.defineValue("png", "color-type", "6");
	try {
		crop_image.write(path.string());
		if (cmd->verbosity >= 1) std::cout << "Finished writing image file: " << path << std::endl;
	} catch (Magick::Exception e) {
		if (cmd->verbosity >= 1) std::cout << "Failed to write image file: " << path << std::endl;
	}
}