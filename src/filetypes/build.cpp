/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "build.hpp"
#include <iostream>
#include <fstream>
#include "../binary_io.hpp"

#include "../cmdlinehelper.hpp"
#include "scml.hpp"

constexpr auto MAGIC = "BILD";

build::build(const fs::path& path, parsed_command_line * cmd) : cmd(cmd) {
	if (cmd->verbosity >= 1) std::cout << "Reading bild file: " << path << std::endl;
	binary_io<std::ifstream> bildfile{Endianess::LITTLE_ENDIAN};
	bildfile.stream.open(path, std::ios::in | std::ios::binary);
	if (bildfile.stream.is_open()) {
		if (cmd->verbosity >= 1) std::cout << "Opened bild file: " << path << std::endl;
		std::string magic;
		bildfile.read_len_string(magic, 4);
		if (magic != MAGIC) {
			throw ZToolsError(strformat("Invalid bild HEADER, expected: \"%s\", got: \"%s\"", MAGIC, magic.c_str()));
		}

		uint32_t symbolcount = 0;
		uint32_t framecount = 0;
		uint32_t alphavertcount = 0;

		bildfile.read_numeric(version);
		bildfile.read_numeric(numsymbols);
		bildfile.read_numeric(numframes);
		bildfile.read_kstring(buildname);

		symbollist[strhash(buildname)] = buildname;

		bildfile.read_numeric(numatlases);

		atlaslist.resize(numatlases);

		for (auto & a : atlaslist) {
			bildfile.read_kstring(a);
		}

		symbols.resize(numsymbols);

		for (auto & s : symbols) {
			symbolcount++;
			bildfile.read_numeric(s.hash);
			bildfile.read_numeric(s.numframes);
			s.frames.resize(s.numframes);
			for (auto & f : s.frames) {
				framecount++;
				bildfile.read_numeric(f.framenumber);
				bildfile.read_numeric(f.duration);
				bildfile.read_numeric(f.x);
				bildfile.read_numeric(f.y);
				bildfile.read_numeric(f.w);
				bildfile.read_numeric(f.h);
				bildfile.read_numeric(f.alphaidx);
				bildfile.read_numeric(f.alphacount);
				alphavertcount += f.alphacount;
				if (f.alphacount % 6 != 0) {
					throw ZToolsError("Corrupted build file (frame VB count should be a multiple of 6).");
				}
			}
		}

		bildfile.read_numeric(numalphaverts);

		if (symbolcount != numsymbols) {
			throw ZToolsError(strformat("Invalid bild HEADER, expected %u symbols got %u symbols.", numsymbols, symbolcount));
		}

		if (framecount != numframes) {
			throw ZToolsError(strformat("Invalid bild HEADER, expected %u frames got %u frames.", numframes, framecount));
		}

		if (alphavertcount != numalphaverts) {
			throw ZToolsError(strformat("Invalid bild HEADER, expected %u alphaverts got %u alphaverts.", numalphaverts, alphavertcount));
		}

		//post load
		for (auto & s : symbols) {
			for (auto & f : s.frames) {
				f.triangles.resize(f.alphacount / 3);
				for (auto & t : f.triangles) {
					bildfile.read_numeric(t.point1.x);
					bildfile.read_numeric(t.point1.y);
					bildfile.read_numeric(t.point1.z);
					bildfile.read_numeric(t.point1.u);
					bildfile.read_numeric(t.point1.v);
					bildfile.read_numeric(t.point1.w);

					bildfile.read_numeric(t.point2.x);
					bildfile.read_numeric(t.point2.y);
					bildfile.read_numeric(t.point2.z);
					bildfile.read_numeric(t.point2.u);
					bildfile.read_numeric(t.point2.v);
					bildfile.read_numeric(t.point2.w);

					bildfile.read_numeric(t.point3.x);
					bildfile.read_numeric(t.point3.y);
					bildfile.read_numeric(t.point3.z);
					bildfile.read_numeric(t.point3.u);
					bildfile.read_numeric(t.point3.v);
					bildfile.read_numeric(t.point3.w);
				}
			}
		}

		bildfile.read_numeric(hashcount);

		hashlist.resize(hashcount);

		for (auto & hash : hashlist) {
			std::string name;
			bildfile.read_numeric(hash);
			bildfile.read_kstring(name);
			symbollist[hash] = name;
		}

		bildfile.stream.close();
	}
}

build::build(const scml scmlfile, parsed_command_line * cmd) : cmd(cmd) {

}

void build::write(const fs::path& path) {
	if (cmd->verbosity >= 1) std::cout << "Writing bild file: " << path << std::endl;
	binary_io<std::ofstream> bildfile{Endianess::LITTLE_ENDIAN};
	bildfile.stream.open(path, std::ios::out | std::ios::binary);
	if (bildfile.stream.is_open()) {
		if (cmd->verbosity >= 1) std::cout << "Opened bild file: " << path << std::endl;
		std::string magic{MAGIC};
		bildfile.write_len_string(magic, 4);

		bildfile.write_numeric(version);
		bildfile.write_numeric(numsymbols);
		bildfile.write_numeric(numframes);

		//cheaty hack to allow renaming the buildname.
		buildname = getrealstring(buildname);

		bildfile.write_kstring(buildname);

		bildfile.write_numeric(numatlases);

		for (auto & a : atlaslist) {
			bildfile.write_kstring(a);
		}

		for (auto & s : symbols) {
			uint32_t hash = strhash(getrealstring(s.hash));
			bildfile.write_numeric(hash);
			bildfile.write_numeric(s.numframes);
			for (auto & f : s.frames) {
				bildfile.write_numeric(f.framenumber);
				bildfile.write_numeric(f.duration);
				bildfile.write_numeric(f.x);
				bildfile.write_numeric(f.y);
				bildfile.write_numeric(f.w);
				bildfile.write_numeric(f.h);
				bildfile.write_numeric(f.alphaidx);
				bildfile.write_numeric(f.alphacount);
			}
		}

		bildfile.write_numeric(numalphaverts);

		//post load
		for (auto & s : symbols) {
			for (auto & f : s.frames) {
				for (auto & t : f.triangles) {
					bildfile.write_numeric(t.point1.x);
					bildfile.write_numeric(t.point1.y);
					bildfile.write_numeric(t.point1.z);
					bildfile.write_numeric(t.point1.u);
					bildfile.write_numeric(t.point1.v);
					bildfile.write_numeric(t.point1.w);

					bildfile.write_numeric(t.point2.x);
					bildfile.write_numeric(t.point2.y);
					bildfile.write_numeric(t.point2.z);
					bildfile.write_numeric(t.point2.u);
					bildfile.write_numeric(t.point2.v);
					bildfile.write_numeric(t.point2.w);

					bildfile.write_numeric(t.point3.x);
					bildfile.write_numeric(t.point3.y);
					bildfile.write_numeric(t.point3.z);
					bildfile.write_numeric(t.point3.u);
					bildfile.write_numeric(t.point3.v);
					bildfile.write_numeric(t.point3.w);
				}
			}
		}

		bildfile.write_numeric(hashcount);

		for (auto & hash : hashlist) {
			uint32_t realhash = strhash(getrealstring(hash));
			bildfile.write_numeric(realhash);
			bildfile.write_kstring(symbollist[hash]);
		}

		bildfile.stream.close();
	}
}

void build::swapsymbol(const std::string & oldsymbol, const std::string & newsymbol) {
	uint32_t oldhash = strhash(oldsymbol);
	symbollist[oldhash] = newsymbol;
}

std::list<std::string> build::getsymbols() {
	std::list<std::string> symbol_list;
	for (auto it = symbollist.begin(); it != symbollist.end(); it++) {
		symbol_list.push_back(it->second);
	}
	return symbol_list;
}

std::string build::getrealstring(uint32_t hash) {
	return symbollist[hash];
}

std::string build::getrealstring(std::string name) {
	return getrealstring(strhash(name));
}