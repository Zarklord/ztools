/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "scml.hpp"
#include <iostream>
#include <fstream>
#include "../binary_io.hpp"
#include "../tinyxml2.h"

#include "../cmdlinehelper.hpp"
#include "build.hpp"
#include "anim.hpp"

#define XMLELEMENTLOOP(elementname, parent) for (tinyxml2::XMLElement * ##elementname = parent->FirstChildElement(#elementname); ##elementname; ##elementname = ##elementname->NextSiblingElement(#elementname))

inline std::string StringAttribute(tinyxml2::XMLElement * elem, const char * attributename, const char * defaultvalue) {
	const char * cattribute = elem->Attribute(attributename);
	std::string attribute = cattribute ? cattribute : defaultvalue;
	return attribute;
}

scml::scml(const fs::path& path, parsed_command_line * cmd) : cmd(cmd) {
	tinyxml2::XMLDocument doc;
	doc.LoadFile(path.string().c_str());
	if (doc.ErrorID() != 0) {
		throw ZToolsError(strformat("Failed to load \"%s\", are you sure it exists?", path.string().c_str()));
	}
	tinyxml2::XMLElement * root = doc.FirstChildElement("spriter_data");
	if (!root) {
		throw ZToolsError("Invalid spriter xml expected a \"spriter_data\" element, but found none.");
	}

	XMLELEMENTLOOP(folder, root) {
		folders.emplace_back();
		scmlfolder & fol = folders.back();

		fol.id = folder->IntAttribute("id", 0);
		fol.name = StringAttribute(folder, "name", "");
		XMLELEMENTLOOP(file, folder) {
			fol.files.emplace_back();
			scmlfolder::scmlfile & fil = fol.files.back();

			fil.type = StringAttribute(file, "type", "image");
			fil.id = file->IntAttribute("id", 0);
			fil.name = StringAttribute(file, "name", "");
			fil.pivot_x = file->FloatAttribute("pivot_x", 0.0f);
			fil.pivot_y = file->FloatAttribute("pivot_y", 0.0f);
			fil.width = file->IntAttribute("width", 0);
			fil.height = file->IntAttribute("height", 0);
			fil.atlas_x = file->IntAttribute("atlas_x", 0);
			fil.atlas_y = file->IntAttribute("atlas_y", 0);
			fil.offset_x = file->IntAttribute("offset_x", 0);
			fil.offset_y = file->IntAttribute("offset_y", 0);
			fil.original_width = file->IntAttribute("original_width", 0);
			fil.original_height = file->IntAttribute("original_height", 0);
		}
	}

	XMLELEMENTLOOP(atlas, root) {
		atlases.emplace_back();
		scmlatlas & atl = atlases.back();

		atl.id = atlas->IntAttribute("id", 0);
		atl.data_path = StringAttribute(atlas, "data_path", "");
		atl.image_path = StringAttribute(atlas, "image_path", "");

		XMLELEMENTLOOP(folder, atlas) {
			atl.folders.emplace_back();
			scmlatlas::scmlfolder & fol = atl.folders.back();

			fol.id = folder->IntAttribute("id", 0);
			fol.name = StringAttribute(folder, "name", "");
			XMLELEMENTLOOP(image, folder) {
				fol.images.emplace_back();
				scmlatlas::scmlfolder::scmlimage & img = fol.images.back();

				img.id = image->IntAttribute("id", 0);
				img.full_path = StringAttribute(image, "full_path", "");
			}
		}
	}

	XMLELEMENTLOOP(entity, root) {
		uint32_t id = entity->IntAttribute("id", 0);
		std::string name = StringAttribute(entity, "name", "");

		tinyxml2::XMLElement * meta_data = entity->FirstChildElement("meta_data");
		if (meta_data) {
			//TODO: load this.
		}

		XMLELEMENTLOOP(animation, entity) {
			uint32_t animation_id = animation->IntAttribute("id", 0);
			std::string animation_name = StringAttribute(animation, "name", "");
			uint32_t length = animation->IntAttribute("length", 0);
			std::string looping = StringAttribute(animation, "looping", "true");
			uint32_t loop_to = animation->IntAttribute("loop_to", 0);

			tinyxml2::XMLElement * animation_meta_data = animation->FirstChildElement("meta_data");
			if (animation_meta_data) {
				//TODO: load this.
			}

			tinyxml2::XMLElement * mainline = animation->FirstChildElement("mainline");
			if (mainline) {
				XMLELEMENTLOOP(key, mainline) {
					uint32_t key_id = key->IntAttribute("id", 0);
					uint32_t time = key->IntAttribute("time", 0);
					tinyxml2::XMLElement * key_meta_data = animation->FirstChildElement("meta_data");
					if (key_meta_data) {
						//TODO: load this.
					}

					XMLELEMENTLOOP(bone, key) {

					}

					XMLELEMENTLOOP(bone_ref, key) {

					}

					XMLELEMENTLOOP(object, key) {

					}

					XMLELEMENTLOOP(object_ref, key) {

					}
				}
			}
			XMLELEMENTLOOP(timeline, animation) {

			}
		}
	}

	XMLELEMENTLOOP(character_map, root) {
		character_maps.emplace_back();
		scmlcharacter_map & char_map = character_maps.back();

		char_map.id = character_map->IntAttribute("id", 0);
		char_map.name = StringAttribute(character_map, "name", "");
		tinyxml2::XMLElement * map = character_map->FirstChildElement("map");
		if (map) {
			char_map.map.atlas = map->IntAttribute("atlas", 0);
			char_map.map.folder = map->IntAttribute("folder", 0);
			char_map.map.file = map->IntAttribute("file", 0);
			char_map.map.target_atlas = map->IntAttribute("target_atlas", 0);
			char_map.map.target_folder = map->IntAttribute("target_folder", 0);
			char_map.map.target_file = map->IntAttribute("target_file", 0);
		}
	}

	tinyxml2::XMLElement * document_info = root->FirstChildElement("document_info");
	if (document_info) {
		doc_info.author = StringAttribute(document_info, "author", "author not specified");
		doc_info.copyright = StringAttribute(document_info, "copyright", "copyright info not specified");
		doc_info.license = StringAttribute(document_info, "license", "no license specified");
		doc_info.version = StringAttribute(document_info, "version", "version not specified");
		doc_info.last_modified = StringAttribute(document_info, "last_modified", "date and time not included");
		doc_info.notes = StringAttribute(document_info, "notes", "no additional notes");
	}

	doc.Clear();
}

scml::scml(const anim anims, const build bild, parsed_command_line * cmd) : cmd(cmd) {

}

void scml::write(const fs::path& path) {

}