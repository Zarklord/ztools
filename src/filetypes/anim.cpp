/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "anim.hpp"
#include <iostream>
#include <fstream>
#include "../binary_io.hpp"

#include "../cmdlinehelper.hpp"
#include "scml.hpp"

constexpr auto MAGIC = "ANIM";

anim::anim(const fs::path & path, parsed_command_line * cmd) : cmd(cmd) {
	if (cmd->verbosity >= 1) std::cout << "Reading anim file: " << path << std::endl;
	binary_io<std::ifstream> animfile{Endianess::LITTLE_ENDIAN};
	animfile.stream.open(path, std::ios::in | std::ios::binary);
	if (animfile.stream.is_open()) {
		if (cmd->verbosity >= 1) std::cout << "Opened anim file: " << path << std::endl;
		std::string magic;
		animfile.read_len_string(magic, 4);
		if (magic != MAGIC) {
			throw ZToolsError(strformat("Invalid anim HEADER, expected: \"%s\", got: \"%s\"", MAGIC, magic.c_str()));
		}

		uint32_t elementcount = 0;
		uint32_t framecount = 0;
		uint32_t eventcount = 0;
		uint32_t animcount = 0;

		animfile.read_numeric(version);
		animfile.read_numeric(numelements);
		animfile.read_numeric(numframes);
		animfile.read_numeric(numevents);
		animfile.read_numeric(numanims);

		animations.resize(numanims);

		for (auto & a : animations) {
			animcount++;
			animfile.read_kstring(a.name);
			symbollist[strhash(a.name)] = a.name;
			animfile.read_numeric(a.direction);
			animfile.read_numeric(a.bankhash);
			animfile.read_numeric(a.framerate);
			animfile.read_numeric(a.framecount);
			a.frames.resize(a.framecount);
			for (auto & f : a.frames) {
				framecount++;
				animfile.read_numeric(f.x);
				animfile.read_numeric(f.y);
				animfile.read_numeric(f.w);
				animfile.read_numeric(f.h);
				//events are depreciated.
				animfile.read_numeric(f.numevents);
				animfile.stream.ignore(4*f.numevents);
				f.numevents = 0;
				animfile.read_numeric(f.numelements);
				f.elements.resize(f.numelements);
				for (auto & e : f.elements) {
					elementcount++;
					animfile.read_numeric(e.hash);
					animfile.read_numeric(e.buildframe);
					animfile.read_numeric(e.layernamehash);
					animfile.read_numeric(e.ma);
					animfile.read_numeric(e.mb);
					animfile.read_numeric(e.mc);
					animfile.read_numeric(e.md);
					animfile.read_numeric(e.mtx);
					animfile.read_numeric(e.mty);
					animfile.read_numeric(e.z);
				}
			}
		}

		if (elementcount != numelements) {
			throw ZToolsError(strformat("Invalid anim HEADER, expected %u elements got %u elements.", numelements, elementcount));
		}

		if (framecount != numframes) {
			throw ZToolsError(strformat("Invalid anim HEADER, expected %u frames got %u frames.", numframes, framecount));
		}

		if (eventcount != numevents) {
			throw ZToolsError(strformat("Invalid anim HEADER, expected %u events got %u events.", numevents, eventcount));
		}

		if (animcount != numanims) {
			throw ZToolsError(strformat("Invalid anim HEADER, expected %u anims got %u anims.", numanims, animcount));
		}

		animfile.read_numeric(hashcount);

		hashlist.resize(hashcount);

		for (auto & hash : hashlist) {
			std::string name;
			animfile.read_numeric(hash);
			animfile.read_kstring(name);
			symbollist[hash] = name;
		}

		animfile.stream.close();
	}
}

anim::anim(const scml scmlfile, parsed_command_line * cmd) : cmd(cmd) {

}

void anim::write(const fs::path & path) {
	if (cmd->verbosity >= 1) std::cout << "Writing anim file: " << path << std::endl;
	binary_io<std::ofstream> animfile{Endianess::LITTLE_ENDIAN};
	animfile.stream.open(path, std::ios::out | std::ios::binary);
	if (animfile.stream.is_open()) {
		if (cmd->verbosity >= 1) std::cout << "Opened anim file: " << path << std::endl;
		std::string magic{MAGIC};
		animfile.write_len_string(magic, 4);

		animfile.write_numeric(version);
		animfile.write_numeric(numelements);
		animfile.write_numeric(numframes);
		animfile.write_numeric(numevents);
		animfile.write_numeric(numanims);

		for (auto & a : animations) {
			std::string name = getrealstring(a.name);
			animfile.write_kstring(a.name);
			animfile.write_numeric(a.direction);
			uint32_t bankhash = strhash(getrealstring(a.bankhash));
			animfile.write_numeric(bankhash);
			animfile.write_numeric(a.framerate);
			animfile.write_numeric(a.framecount);
			for (auto & f : a.frames) {
				animfile.write_numeric(f.x);
				animfile.write_numeric(f.y);
				animfile.write_numeric(f.w);
				animfile.write_numeric(f.h);
				//events are no longer used, thanks Scott :)
				f.numevents = 0;
				animfile.write_numeric(f.numevents);
				/*for (auto & e : f.events) {
					animfile.write_numeric(e);
				}*/
				animfile.write_numeric(f.numelements);
				for (auto & e : f.elements) {
					uint32_t hash = strhash(getrealstring(e.hash));
					animfile.write_numeric(hash);
					animfile.write_numeric(e.buildframe);
					uint32_t layernamehash = strhash(getrealstring(e.layernamehash));
					animfile.write_numeric(layernamehash);
					animfile.write_numeric(e.ma);
					animfile.write_numeric(e.mb);
					animfile.write_numeric(e.mc);
					animfile.write_numeric(e.md);
					animfile.write_numeric(e.mtx);
					animfile.write_numeric(e.mty);
					animfile.write_numeric(e.z);
				}
			}
		}

		animfile.write_numeric(hashcount);

		for (auto & hash : hashlist) {
			uint32_t realhash = strhash(getrealstring(hash));
			animfile.write_numeric(realhash);
			animfile.write_kstring(symbollist[hash]);
		}

		animfile.stream.close();
	}
}

void anim::swapsymbol(const std::string & oldsymbol, const std::string & newsymbol) {
	uint32_t oldhash = strhash(oldsymbol);
	symbollist[oldhash] = newsymbol;
}

std::list<std::string> anim::getsymbols() {
	std::list<std::string> symbols;
	for (auto it = symbollist.begin(); it != symbollist.end(); it++) {
		symbols.push_back(it->second);
	}
	return symbols;
}

std::string anim::getrealstring(uint32_t hash) {
	return symbollist[hash];
}

std::string anim::getrealstring(std::string name) {
	return getrealstring(strhash(name));
}
