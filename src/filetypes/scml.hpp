/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "../common.hpp"

class anim;
class build;
class parsed_command_line;

class scml {
	friend class anim;
	friend class build;
	public:
		scml(const fs::path&path, parsed_command_line * cmd);
		scml(const anim anims, build bild, parsed_command_line * cmd);
		void write(const fs::path& path);
	private:

		parsed_command_line * cmd;


		struct scmlfolder {
			uint32_t id;
			std::string name;
			struct scmlfile {
				std::string type;
				uint32_t id;
				std::string name;
				float pivot_x;
				float pivot_y;
				uint32_t width;
				uint32_t height;
				uint32_t atlas_x;
				uint32_t atlas_y;
				uint32_t offset_x;
				uint32_t offset_y;
				uint32_t original_width;
				uint32_t original_height;
			};
			std::vector<scmlfile> files;
		};

		struct scmlatlas {
			uint32_t id;
			std::string data_path;
			std::string image_path;
			struct scmlfolder {
				uint32_t id;
				std::string name;
				struct scmlimage {
					uint32_t id;
					std::string full_path;
				};
				std::vector<scmlimage> images;
			};
			std::vector<scmlfolder> folders;
		};

		struct scmlentity {

		};

		struct scmlcharacter_map {
			uint32_t id;
			std::string name;
			struct scmlmap {
				uint32_t atlas;
				uint32_t folder;
				uint32_t file;
				uint32_t target_atlas;
				uint32_t target_folder;
				uint32_t target_file;
			};
			scmlmap map;
		};

		struct scmldoc_info {
			std::string author;
			std::string copyright;
			std::string license;
			std::string version;
			std::string last_modified;
			std::string notes;
		};

		std::vector<scmlfolder> folders;

		std::vector<scmlatlas> atlases;

		std::vector<scmlentity> entities;

		std::vector<scmlcharacter_map> character_maps;

		scmldoc_info doc_info;
};