/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "atlas.hpp"
#include "../tinyxml2.h"

#include "../cmdlinehelper.hpp"
#include "png.hpp"

#define XMLELEMENTLOOP(elementname, parent) for (tinyxml2::XMLElement * ##elementname = parent->FirstChildElement(#elementname); ##elementname; ##elementname = ##elementname->NextSiblingElement(#elementname))

inline std::string StringAttribute(tinyxml2::XMLElement* elem, const char* attributename, const char* defaultvalue) {
	const char* cattribute = elem->Attribute(attributename);
	std::string attribute = cattribute ? cattribute : defaultvalue;
	return attribute;
}

inline std::list<fs::path> get_image_paths_from_dir(const fs::path& dir) {
	std::list<fs::path> image_paths;
	for (auto& p : fs::directory_iterator(dir)) {
		if (fs::is_regular_file(p)) {
			image_paths.push_back(p.path());
		}
	}
	return image_paths;
}

atlas::atlas(const fs::path& directory, parsed_command_line * cmd) : atlas(get_image_paths_from_dir(directory), cmd) {}

atlas::atlas(const std::list<fs::path>& image_paths, parsed_command_line * cmd) : cmd(cmd) {
	AtlasFill::atlas_data atlas_data = AtlasFill::AtlasFill(image_paths, cmd);
	Magick::DrawableList drawcommands;
	for (auto& image_data : atlas_data.image_datas) {
		drawcommands.push_back(Magick::DrawableCompositeImage(image_data.xoffset, image_data.yoffset, image_data.path.string()));
		imgs.emplace(image_data.path, uv_coords{ image_data.width, image_data.height, image_data.xoffset, image_data.yoffset });
		names.push_back(image_data.path);
	}
	atlas_image = new png{ atlas_data.width, atlas_data.height, drawcommands, cmd };
}

atlas::atlas(const fs::path& xml_path, const fs::path& tex_path, parsed_command_line * cmd) : cmd(cmd) {
	atlas_image = new png{ tex{tex_path, cmd}, cmd };
	auto img_size = atlas_image->size();
	uint16_t atlas_width = static_cast<uint16_t>(img_size.width());
	uint16_t atlas_height = static_cast<uint16_t>(img_size.height());

	float offset_amount = cmd->atlas_pixel_offset;

	float u_offset = offset_amount / atlas_width;
	float v_offset = offset_amount / atlas_height;

	tinyxml2::XMLDocument doc;
	doc.LoadFile(xml_path.string().c_str());
	if (doc.ErrorID() != 0) {
		throw ZToolsError(strformat("Failed to load \"%s\", are you sure it exists?", xml_path.string().c_str()));
	}
	tinyxml2::XMLElement* root = doc.FirstChildElement("Atlas");
	if (!root) {
		throw ZToolsError("Invalid atlas xml expected a \"Atlas\" element, but found none.");
	}
	tinyxml2::XMLElement* Elements = root->FirstChildElement("Elements");
	if (Elements) {
		XMLELEMENTLOOP(Element, Elements) {
			std::string elem_name = StringAttribute(Element, "name", "");
			std::cout << "found element " << elem_name << "\n";
			float u_start = (Element->FloatAttribute("u1", 0.0f) - u_offset) * atlas_width;
			float u_end = (Element->FloatAttribute("u2", 0.0f) + u_offset) * atlas_width;
			//v is stored inverted in the atlas, so we swap it here
			float v_start = (1.0f - (Element->FloatAttribute("v2", 0.0f) + v_offset)) * atlas_height;
			float v_end = (1.0f - (Element->FloatAttribute("v1", 0.0f) - v_offset)) * atlas_height;
			imgs.emplace(elem_name, uv_coords{(uint32_t)(u_end - u_start), (uint32_t)(v_end - v_start), (uint32_t)u_start, (uint32_t)v_start});
			names.push_back(elem_name);
		}
	}
}

atlas::~atlas() {
	delete atlas_image;
}

void atlas::write(const fs::path& directory) {
	for (auto img_name : names) {
		uv_coords img_coords = imgs.at(img_name);
		img_name.replace_extension(".png");
		atlas_image->write(directory / img_name, Magick::Geometry(img_coords.width, img_coords.height, img_coords.xoffset, img_coords.yoffset));
	}
}

void atlas::write(const fs::path& xml_path, const fs::path& tex_path) {
	auto img_size = atlas_image->size();
	float atlas_width = static_cast<float>(img_size.width());
	float atlas_height = static_cast<float>(img_size.height());

	float offset_amount = cmd->atlas_pixel_offset;

	float u_offset = offset_amount / atlas_width;
	float v_offset = offset_amount / atlas_height;

	tinyxml2::XMLDocument doc;
	tinyxml2::XMLElement* root = doc.InsertEndChild(doc.NewElement("Atlas"))->ToElement();
	if (root) {
		tinyxml2::XMLElement* Texture = root->InsertEndChild(doc.NewElement("Texture"))->ToElement();
		if (Texture) {
			std::string filename = tex_path.filename().string();
			Texture->SetAttribute("filename", filename.c_str());
		}
		tinyxml2::XMLElement* Elements = root->InsertEndChild(doc.NewElement("Elements"))->ToElement();
		if (Elements) {
			for (auto& image_uvs : imgs) {
				tinyxml2::XMLElement* Element = doc.NewElement("Element");
				float width = static_cast<float>(image_uvs.second.width);
				float height = static_cast<float>(image_uvs.second.height);
				float xoffset = static_cast<float>(image_uvs.second.xoffset);
				float yoffset = static_cast<float>(image_uvs.second.yoffset);
				float u1 = std::clamp(xoffset / atlas_width + u_offset, 0.0f, 1.0f);
				float u2 = std::clamp((xoffset + width) / atlas_width - u_offset, 0.0f, 1.0f);
				float v1 = std::clamp(1.0f - (yoffset + height) / atlas_height + v_offset, 0.0f, 1.0f);
				float v2 = std::clamp(1.0f - yoffset / atlas_height - v_offset, 0.0f, 1.0f);
				std::string elementname = image_uvs.first.filename().replace_extension(".tex").string();
				Element->SetAttribute("name", elementname.c_str());
				Element->SetAttribute("u1", u1);
				Element->SetAttribute("u2", u2);
				Element->SetAttribute("v1", v1);
				Element->SetAttribute("v2", v2);
				Elements->InsertEndChild(Element);
			}
		}
	}
	doc.SaveFile(xml_path.string().c_str());

	tex texfile{ *atlas_image, cmd };
	texfile.write(tex_path);
}

int atlas::imagecount() {
	return names.size();
}
