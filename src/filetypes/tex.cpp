/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tex.hpp"
#include <iostream>
#include <fstream>
#include "squish/squish.h"
#include "../binary_io.hpp"

#include "../cmdlinehelper.hpp"
#include "png.hpp"

constexpr auto MAGIC = "KTEX";
constexpr auto PRECAVESFILL = 0x0003FFFF;
constexpr auto FILL = 0x0FFF;

static inline bool ispow2(int num) {
	return num && !(num & (num - 1));
}

tex::tex(const fs::path& path, parsed_command_line* cmd) : cmd{ cmd }, flags{ 0 } {
	if (cmd->verbosity >= 1) std::cout << "Reading tex file: " << path << std::endl;
	binary_io<std::ifstream> texfile{Endianess::LITTLE_ENDIAN};
	texfile.stream.open(path, std::ios::in | std::ios::binary);
	if (texfile.stream.is_open()) {
		if (cmd->verbosity >= 1) std::cout << "Opened tex file: " << path << std::endl;
		std::string magic;
		texfile.read_len_string(magic, 4);
		if (magic != MAGIC) {
			throw ZToolsError(strformat("Invalid tex HEADER, expected: \"%s\", got: \"%s\"", MAGIC, magic.c_str()));
		}
		texfile.read_numeric(flags.data);

		if (cmd->pre_caves) {
			flags.spec.fill = flags.pre_caves_spec.fill;
			flags.spec.flags = flags.pre_caves_spec.flags;
			flags.spec.mipmap_count = flags.pre_caves_spec.mipmap_count;
			flags.spec.texture_type = flags.pre_caves_spec.texture_type;
			flags.spec.compression = flags.pre_caves_spec.compression;
			flags.spec.platform = flags.pre_caves_spec.platform;
		}

		if ((flags.spec.fill & FILL) != FILL) {
			texfile.reorder(flags.data);
			if ((flags.spec.fill & FILL) == FILL) {
				texfile.setEndian(Endianess::BIG_ENDIAN);
			} else {
				throw ZToolsError("Invalid tex HEADER, FILL wasn't FILLED");
			}
		}

		bool iscompressed;
		int squishcompression;
		getcompressionflags(flags.spec.compression, iscompressed, squishcompression);

		if (flags.spec.compression == compression_types::RGB) datafmt = "RGB";
		else datafmt = "RGBA";

		mipmaps.resize(flags.spec.mipmap_count);

		for (auto & mip : mipmaps) {
			texfile.read_numeric(mip.width);
			texfile.read_numeric(mip.height);
			texfile.read_numeric(mip.pitch);
			texfile.read_numeric(mip.datasize);
		}

		for (auto & mip : mipmaps) {
			uint32_t ds = mip.datasize;
			char * mip_data = new char[ds];
			texfile.stream.read(mip_data, ds);

			if (iscompressed) {
				squish::u8 * rgba = new squish::u8[4 * mip.width*mip.height];
				squish::DecompressImage(rgba, mip.width, mip.height, mip_data, squishcompression);
				mip.data.update(rgba, 4 * mip.width*mip.height);
				delete[] rgba;
			} else {
				mip.data.update(mip_data, ds);
			}
			delete[] mip_data;
		}

		texfile.stream.close();
	}
}

tex::tex(const png& texture, parsed_command_line * cmd) : cmd{ cmd } {
	png to_tex = texture;
	flags.spec.platform = platforms::DEFAULT;
	flags.spec.compression = cmd->compression_type;
	flags.spec.texture_type = cmd->texture_type;
	flags.spec.flags = 3;
	flags.spec.fill = FILL;

	Magick::Geometry size = to_tex.image.size();
	if (!ispow2(size.width()) || !ispow2(size.height())) {
		throw ZToolsError("The PNG files Width/Height is not a power of 2!");
	}

	if (flags.spec.compression == compression_types::RGB) datafmt = "RGB";
	else datafmt = "RGBA";

	to_tex.image.depth(8);
	to_tex.image.flip();

	mipmaps.resize(mipmaps.size() + 1);
	mipmap& primarymip = mipmaps.back();
	primarymip.width = static_cast<uint16_t>(size.width());
	primarymip.height = static_cast<uint16_t>(size.height());
	primarymip.pitch = ((primarymip.width + 3) / 4) * (datafmt == "RGB" ? 3 : 4) * 4;
	to_tex.image.write(&primarymip.data, datafmt);
	primarymip.datasize = primarymip.data.length() / (datafmt == "RGBA" ? 4 : 3);
	primarymip.datasize = std::max(primarymip.datasize, 16u);
	if (!cmd->no_mipmaps) {
		int width = size.width(), height = size.height();
		while (width > 1 || height > 1) {
			if (width > 1) width >>= 1;
			if (height > 1) height >>= 1;

			std::vector<Magick::Image> separatedImages;
			Magick::separateImages(&separatedImages, to_tex.image, Magick::AllChannels);
			for (auto& image : separatedImages) {
				image.filterType(cmd->resize_type);
				image.resize(Magick::Geometry(std::max(width, 1), std::max(height, 1)));
			}
			Magick::Image combinedImage;
			Magick::combineImages(&combinedImage, separatedImages.begin(), separatedImages.end(), Magick::AllChannels);

			mipmaps.resize(mipmaps.size() + 1);
			mipmap& mip = mipmaps.back();
			//width
			mip.width = static_cast<uint16_t>(width);
			//height
			mip.height = static_cast<uint16_t>(height);
			//pitch
			mip.pitch = ((mip.width + 3) / 4) * (datafmt == "RGB" ? 3 : 4) * 4;
			//data
			combinedImage.write(&mip.data, datafmt);
			//datasize
			mip.datasize = mip.data.length() / (datafmt == "RGBA" ? 4 : 3);
			mip.datasize = std::max(mip.datasize, 16u);
		}
	}

	if (!cmd->no_premultiply && datafmt == "RGBA") {
		for (auto & m : mipmaps) {
			uint8_t * imagedata = const_cast<uint8_t*>(reinterpret_cast<const uint8_t*>(m.data.data()));
			for (uint32_t idx = 0; idx < m.data.length(); idx += 4) {
				uint8_t * pixel = &imagedata[idx];
				pixel[0] = pixel[0] * float(pixel[3] / 255.0f);
				pixel[1] = pixel[1] * float(pixel[3] / 255.0f);
				pixel[2] = pixel[2] * float(pixel[3] / 255.0f);
			}
		}
	}
	flags.spec.mipmap_count = mipmaps.size();
}

void tex::getcompressionflags(int compression, bool &iscompressed, int &squishcompression) {
	iscompressed = compression == compression_types::DXT1 || compression == compression_types::DXT3 || compression == compression_types::DXT5;

	squishcompression = -1;
	if (iscompressed) {
		if (compression == compression_types::DXT1) {
			squishcompression = squish::kDxt1;
		} else if (compression == compression_types::DXT3) {
			squishcompression = squish::kDxt3;
		} else if (compression == compression_types::DXT5) {
			squishcompression = squish::kDxt5;
		}
	}
}

void tex::write(const fs::path& path) {
	if (cmd->verbosity >= 1) std::cout << "Writing tex file: " << path << std::endl;
	binary_io<std::ofstream> texfile{ Endianess::LITTLE_ENDIAN };
	texfile.stream.open(path, std::ios::out | std::ios::binary);
	if (texfile.stream.is_open()) {
		if (cmd->verbosity >= 1) std::cout << "Opened tex file: " << path << std::endl;
		std::string magic{ MAGIC };
		texfile.write_len_string(magic, 4);
		texfile.write_numeric(flags.data);

		for (auto& mip : mipmaps) {
			texfile.write_numeric(mip.width);
			texfile.write_numeric(mip.height);
			texfile.write_numeric(mip.pitch);
			texfile.write_numeric(mip.datasize);
		}

		bool iscompressed;
		int squishcompression;
		getcompressionflags(flags.spec.compression, iscompressed, squishcompression);

		for (auto& mip : mipmaps) {
			if (iscompressed) {
				int datasize = squish::GetStorageRequirements(mip.width, mip.height, squishcompression);
				char* compresseddata = new char[datasize];
				squish::CompressImage(reinterpret_cast<const squish::u8*>(mip.data.data()), mip.width, mip.height, compresseddata, squishcompression);
				texfile.stream.write(compresseddata, datasize);
				delete[] compresseddata;
			}
			else {
				texfile.stream.write(reinterpret_cast<const char*>(mip.data.data()), mip.datasize);
			}
		}

		texfile.stream.close();
	}
}

void tex::write(const fs::path& path, const int16_t miplevel) {
	int start = miplevel == -1 ? 0 : std::min(miplevel, (int16_t)getmipcount());
	int end = miplevel == -1 ? mipmaps.size() : start + 1;
	for (int i = start; i < end; i++) {
		auto& mip = mipmaps[i];
		Magick::Image image;
		image.read(mip.data, Magick::Geometry(mip.width, mip.height), 8, datafmt);
		image.flip();
		image.magick("png");
		image.defineValue("png", "color-type", "6");
		std::string filename{ path.filename().string() };
		filename.insert(filename.length() - 4, "_" + std::to_string(mip.width) + "x" + std::to_string(mip.height));
		fs::path newpath = path.parent_path() / filename;

		//std::cout << newpath << "\n";
		image.write(newpath.string());
	}
}