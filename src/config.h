#pragma once

/* #undef HAVE_LIBZIP */

/* define if the library defines strstream */
/* #undef HAVE_CLASS_STRSTREAM */

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H

/* define if the C++ implementation have long long */
#define HAVE_LONG_LONG

#define HAVE_CSTDDEF

#define HAVE_STDDEF_H

/* define if the compiler has stringstream */
#define HAVE_SSTREAM

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H

/* Define to 1 if you have the <strstream> header file. */
/* #undef HAVE_STRSTREAM */

/* Define to 1 if you have the <sys/stat.h> header file. */
/* #undef HAVE_SYS_STAT_H */

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H

/* Define to 1 if you have the <unistd.h> header file. */
/* #undef HAVE_UNISTD_H */

#define HAVE_SNPRINTF

/* #undef HAVE__SNPRINTF */

/* #undef HAVE_MODE_T */

/* #undef HAVE_RESTRICT */
/* #undef HAVE_UURESTRICT */
/* #undef HAVE_UURESTRICTUU */

/* Define to the full name of this package. */
/* #undef PACKAGE_NAME */

/* Define to the full name and version of this package. */
/* #undef PACKAGE_STRING */

/* Define to the version of this package. */
#define PACKAGE_VERSION "2.0.3"

/* Define to the type of an unsigned integer type of width exactly 16 bits if
   such a type exists and the standard includes do not define it. */
/* #undef uint16_t */

/* Define to the type of an unsigned integer type of width exactly 32 bits if
   such a type exists and the standard includes do not define it. */
/* #undef uint32_t */

/* Define to the type of an unsigned integer type of width exactly 64 bits if
   such a type exists and the standard includes do not define it. */
/* #undef uint64_t */

/* Define to the type of an signed integer type of width exactly 16 bits if
   such a type exists and the standard includes do not define it. */
/* #undef int16_t */

/* Define to the type of an signed integer type of width exactly 32 bits if
   such a type exists and the standard includes do not define it. */
/* #undef int32_t */

/* Define to the type of an signed integer type of width exactly 64 bits if
   such a type exists and the standard includes do not define it. */
/* #undef int64_t */

/* #undef size_t */

#define ssize_t long

#if defined(ssize_t)
	typedef ssize_t ssize_t_WORKAROUND;
#	undef ssize_t
	typedef ssize_t_WORKAROUND ssize_t;
#endif
