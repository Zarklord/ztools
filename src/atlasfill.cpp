/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "atlasfill.hpp"
#include "cmdlinehelper.hpp"

namespace AtlasFill {

	struct empty_space {
		uint16_t x;
		uint16_t y;
		uint16_t w;
		uint16_t h;
	};

	inline uint16_t max_side(const atlas_data::atlas_image_data& a) {
		return std::max(a.width, a.height);
	}

	inline uint16_t min_side(const atlas_data::atlas_image_data& a) {
		return std::min(a.width, a.height);
	}

	inline uint16_t area(const atlas_data::atlas_image_data& a) {
		return a.width * a.height;
	}

	inline uint16_t perimeter(const atlas_data::atlas_image_data& a) {
		return a.width * 2 + a.height * 2;
	}

	inline float pathological(const atlas_data::atlas_image_data& a) {
		return float(max_side(a)) / min_side(a) * area(a);
	}

	bool sort_area(const atlas_data::atlas_image_data& first, const atlas_data::atlas_image_data& second) {
		return area(first) > area(second);
	}
	bool sort_width(const atlas_data::atlas_image_data& first, const atlas_data::atlas_image_data& second) {
		return first.width == second.width && first.height > second.height || first.width > second.width;
	}
	bool sort_height(const atlas_data::atlas_image_data& first, const atlas_data::atlas_image_data& second) {
		return first.height == second.height && first.width > second.width || first.height > second.height;
	}
	bool sort_side(const atlas_data::atlas_image_data& first, const atlas_data::atlas_image_data& second) {
		return max_side(first) > max_side(second);
	}
	bool sort_perimeter(const atlas_data::atlas_image_data& first, const atlas_data::atlas_image_data& second) {
		return perimeter(first) > perimeter(second);
	}
	bool sort_pathological(const atlas_data::atlas_image_data& first, const atlas_data::atlas_image_data& second) {
		return pathological(first) > pathological(second);
	}
	typedef bool sort_atlas_fn(const atlas_data::atlas_image_data& first, const atlas_data::atlas_image_data& second);
	static std::vector<sort_atlas_fn*> sort_atlas_fns{ sort_area, sort_width, sort_height, sort_side, sort_perimeter, sort_pathological };

	inline void insert_empty_sorted(std::list<empty_space>& empty_spaces, empty_space& empty) {
		for (auto empty_space_iter = empty_spaces.begin(); empty_space_iter != empty_spaces.end(); ++empty_space_iter) {
			if (empty.w * empty.h < empty_space_iter->w * empty_space_iter->h) {
				empty_spaces.insert(empty_space_iter, empty);
				return;
			}
		}
		empty_spaces.push_back(empty);
	}

	atlas_data AtlasFill(std::list<fs::path> image_paths, parsed_command_line * cmd) {
		atlas_data atlas;

		uint32_t total_image_area = 0;
		uint16_t abs_minimum_width = 0;
		uint16_t abs_minimum_height = 0;
		Magick::Image imagedata;
		image_paths.sort();
		for (fs::path& image_path : image_paths) {
			if (cmd->verbosity >= 1) std::cout << "Pinging image file: " << image_path << std::endl;
			try {
				imagedata.ping(image_path.string());
				if (cmd->verbosity >= 1) std::cout << "Finished pinging image file: " << image_path << std::endl;
				auto size = imagedata.size();
				total_image_area += size.width() * size.height();
				abs_minimum_width = std::max<uint16_t>(abs_minimum_width, (uint16_t)size.width());
				abs_minimum_height = std::max<uint16_t>(abs_minimum_height, (uint16_t)size.height());
				atlas.image_datas.emplace_back(image_path, (uint16_t)size.width(), (uint16_t)size.height(), (uint16_t)0, (uint16_t)0);
			} catch(Magick::Exception e) {
				if (cmd->verbosity >= 1) std::cout << image_path << " is not a image. skipping..." << std::endl;
			} catch (...) {
				std::cout << "Exception occured" << std::endl;
			}
		}

		if (total_image_area > (uint32_t)(cmd->atlas_width * cmd->atlas_height)) {
			throw ZToolsError(strformat("Images cannot fit in a %ux%u atlas, either increase the atlas size or split the images into two atlases!", cmd->atlas_width, cmd->atlas_height));
		}

		abs_minimum_width = upper_power_of_two(abs_minimum_width);
		abs_minimum_height = upper_power_of_two(abs_minimum_height);

		if (abs_minimum_width > cmd->atlas_width || abs_minimum_height > cmd->atlas_height) {
			throw ZToolsError(strformat("Images in an atlas cannot be wider than %u pixels or taller than %u pixels!", cmd->atlas_width, cmd->atlas_height));
		}

		while (total_image_area > (uint32_t)(abs_minimum_width * abs_minimum_height)) {
			if ((abs_minimum_width <= abs_minimum_height || abs_minimum_height == cmd->atlas_height) && abs_minimum_width < cmd->atlas_width) {
				abs_minimum_width *= 2;
			} else if (abs_minimum_height < cmd->atlas_height) {
				abs_minimum_height *= 2;
			}
		}
		if (cmd->verbosity >= 2) std::cout << "atlas_minimum_dimensions are: " << abs_minimum_width << "x" << abs_minimum_height << std::endl;

		atlas_data best_atlas;
		//value double the maximum possible atlas so we are gauranteed that the first valid atlas gets put in here.
		best_atlas.width = cmd->atlas_width * 2;
		best_atlas.height = cmd->atlas_height* 2;
		for (auto& sort_atlas : sort_atlas_fns) {
			bool failed_atlas = true;
			atlas.width = abs_minimum_width;
			atlas.height = abs_minimum_height;
			atlas.image_datas.sort(sort_atlas);
			while (atlas.width <= cmd->atlas_width && atlas.height <= cmd->atlas_height) {
				failed_atlas = true;
				std::list<empty_space> empty_spaces{ empty_space{0, 0, atlas.width, atlas.height} };
				for (auto& atlas_image : atlas.image_datas) {
					failed_atlas = true;
					for (auto empty_space_iter = empty_spaces.begin(); empty_space_iter != empty_spaces.end(); ++empty_space_iter) {
						if (atlas_image.width <= empty_space_iter->w && atlas_image.height <= empty_space_iter->h) {
							failed_atlas = false;

							empty_space current_empty = *empty_space_iter;
							empty_spaces.erase(empty_space_iter);

							atlas_image.xoffset = current_empty.x;
							atlas_image.yoffset = current_empty.y;

							if (atlas_image.width < current_empty.w) {
								empty_space width_empty{ (uint16_t)(current_empty.x + atlas_image.width), current_empty.y, (uint16_t)(current_empty.w - atlas_image.width), atlas_image.height };
								insert_empty_sorted(empty_spaces, width_empty);
							}
							if (atlas_image.height < current_empty.h) {
								empty_space height_empty{ current_empty.x, (uint16_t)(current_empty.y + atlas_image.height), current_empty.w, (uint16_t)(current_empty.h - atlas_image.height) };
								insert_empty_sorted(empty_spaces, height_empty);
							}
							break;
						}
					}
					if (failed_atlas) break;
				}
				if (!failed_atlas) break;
				if (atlas.width <= atlas.height) {
					atlas.width *= 2;
				} else {
					atlas.height *= 2;
				}
			}
			if (!failed_atlas) {
				if (atlas.width == abs_minimum_width && atlas.height == abs_minimum_height) {
					best_atlas = atlas;
					break;
				} else if (atlas.width * atlas.height < best_atlas.width * best_atlas.height) {
					best_atlas = atlas;
				}
			}
		}
		if (best_atlas.width == cmd->atlas_width * 2 && best_atlas.height == cmd->atlas_height * 2) {
			throw ZToolsError(strformat("Unable to fit images in the atlas, either increase the atlas size or split the images into two atlases!", cmd->atlas_width, cmd->atlas_height));
		}
		if (cmd->verbosity >= 2) {
			std::cout << "atlas dimensions are: " << best_atlas.width << "x" << best_atlas.height << std::endl;
			for (auto& atlas_image : best_atlas.image_datas) {
				std::cout << "path: " << atlas_image.path << " offset: " << atlas_image.xoffset << "x" << atlas_image.yoffset << " dimensions: " << atlas_image.width << "x" << atlas_image.height << std::endl;
			}
		}
		return best_atlas;
	}
}