/*
*	This file is part of ZTools.
*
*	ZTools is free software: you can redistribute it and/or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	ZTools is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with ZTools.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "common.hpp"
#include "filetypes/tex.hpp"
#include "atlasfill.hpp"
#include "Magick++.h"
#include <cassert>
#include <algorithm>
#include <map>
#include <tclap/CmdLine.h>

const char option_brackets[2][2] = {{'[', ']'}, {'<', '>'}};

class ZToolsOutput : public TCLAP::StdOutput {
	std::string license;

	// Lists of options categories, in the order they
	// should be printed.
	std::vector<std::string> categories;

	// Inverse map of the above.
	std::map<std::string, size_t> categories_inverse;

	// Map of options to category indexes.
	std::map<const TCLAP::Arg*, size_t> category_map;

	size_t get_arg_category_idx(const TCLAP::Arg* a) const {
		auto it = category_map.find(a);

		// Put it at the end.
		if (it == category_map.end()) {
			return categories.size();
		} else {
			return it->second;
		}
	}

	class arg_comparer;
	friend class arg_comparer;
	class arg_comparer {
		const ZToolsOutput* o;
		public:
		arg_comparer(const ZToolsOutput* _o) : o(_o) {}

		// Comparison based on category order.
		bool operator()(const TCLAP::Arg* a, const TCLAP::Arg* b) const {
			return o->get_arg_category_idx(a) < o->get_arg_category_idx(b);
		}
	};

	static bool isUnlabeled(TCLAP::Arg* a) {
		const std::string& desc = a->shortID();

		return desc.length() <= 1 || desc[1] != TCLAP::Arg::flagStartChar();
	}

	void getSortedArgs(const std::list<TCLAP::Arg*>& args, std::vector<TCLAP::Arg*>& sorted_args) const {
		sorted_args.clear();

		for (std::list<TCLAP::Arg*>::const_iterator it = args.begin(); it != args.end(); ++it) {
			if (!isUnlabeled(*it)) {
				sorted_args.push_back(*it);
			}
		}

		std::stable_sort(sorted_args.begin(), sorted_args.end(), arg_comparer(this));
	}

	protected:
	virtual inline void usage_short(TCLAP::CmdLineInterface& c, std::ostream& out) {
		using namespace std;
		using namespace TCLAP;

		std::list<Arg*> argList = c.getArgList();

		string s = "Usage: " + c.getProgramName() + " [OPTION]... [--]";

		for (ArgListIterator it = argList.begin(); it != argList.end(); ++it) {
			if ((*it)->isRequired() || isUnlabeled(*it)) {
				s += " " + (*it)->shortID();
			}
		}

		spacePrint(out, s, 80, 0, 0);
	}

	virtual inline void usage_long(TCLAP::CmdLineInterface& c, std::ostream& out) {
		using namespace std;
		using namespace TCLAP;

		std::vector<Arg*> sorted_args;
		getSortedArgs(c.getArgList(), sorted_args);

		const size_t nargs = sorted_args.size();
		const size_t ncats = categories.size();

		assert(nargs <= c.getArgList().size());
		(void) nargs;

		// Last category idx;
		size_t last_cat_idx = ncats + 1;

		for (std::vector<Arg*>::const_iterator it = sorted_args.begin(); it != sorted_args.end(); ++it) {
			const size_t cat_idx = get_arg_category_idx(*it);

			if (cat_idx != last_cat_idx) {
				if (cat_idx >= ncats) {
					assert(cat_idx == ncats);
					out << "Other options:" << endl;
				} else {
					out << categories[cat_idx] << ":" << endl;
				}
			}

			spacePrint(out, (*it)->longID(), 80, 4, 2);
			spacePrint(out, (*it)->getDescription(), 80, 9, 0);

			last_cat_idx = cat_idx;
		}
	}

	public:
	ZToolsOutput(const std::string& _license) : license(_license) {}

	void addCategory(const std::string& cat) {
		if (categories_inverse.count(cat)) {
			return;
		}

		categories_inverse.insert(std::make_pair(cat, categories.size()));
		categories.push_back(cat);
	}

	void setArgCategory(const TCLAP::Arg& a, const std::string& cat) {
		auto it = categories_inverse.find(cat);
		if (it == categories_inverse.end()) {
			throw Error("Invalid argument category '" + cat + "'");
		}

		category_map.insert(std::make_pair(&a, it->second));
	}

	virtual inline void usage(TCLAP::CmdLineInterface& c) {
		using namespace std;

		usage_short(c, cout);
		cout << endl;
		usage_long(c, cout);
		cout << endl;
		//spacePrint(cout, c.getMessage(), 80, 0, 0);
		cout << c.getMessage() << endl;
	}

	virtual void version(TCLAP::CmdLineInterface& c) {
		std::cout << c.getProgramName() << " " << c.getVersion() << std::endl;
		std::cout << std::endl;
		std::cout << license << std::endl;
	}

	virtual inline void failure(TCLAP::CmdLineInterface& c, TCLAP::ArgException& e) {
		using namespace std;
		using namespace TCLAP;

		const string& programName = c.getProgramName();
		string prefix = programName + ": ";

		cerr << prefix << e.error() << endl;
		if (e.argId().length() > 0 && e.argId().compare(" ")) {
			cerr << string(prefix.length(), ' ') << e.argId() << endl;
		}
		cerr << endl;
		usage_short(c, cerr);
		cerr << "Try '" << programName << " --help' for more information." << endl;

		throw ExitException(1);
	}
};

class ArgumentOption : public TCLAP::UnlabeledValueArg<std::string> {
	// Override for the visual display of requiredness.
	bool visual_required_override;

	public:
	void setVisuallyRequired(bool b) {
		visual_required_override = b;
	}

	virtual bool processArg(int* i, std::vector<std::string>& args) {
		// POSIX-like behaviour.
		if (!Arg::ignoreRest()) {
			const std::string& s = args[*i];
			if (s.length() == 0 || s[0] == TCLAP::Arg::flagStartChar()) {
				return false;
			}
		}

		return UnlabeledValueArg<std::string>::processArg(i, args);
	}

	virtual std::string shortID(const std::string& val = "") const {
		std::string original = UnlabeledValueArg<std::string>::shortID(val);

		bool required = isRequired() || visual_required_override;

		return std::string(1, option_brackets[required][0])
			+ original.substr(1, original.length() - 2)
			+ std::string(1, option_brackets[required][1]);
	}

	ArgumentOption(const std::string& name, const std::string& desc, bool req, const std::string& typeDesc) :
		UnlabeledValueArg<std::string>(name, desc, req, "", typeDesc) {}
};

class MultiArgumentOption : public TCLAP::UnlabeledMultiArg<std::string> {
	// Override for the visual display of requiredness.
	bool visual_required_override;

	public:
	void setVisuallyRequired(bool b) {
		visual_required_override = b;
	}

	virtual bool processArg(int* i, std::vector<std::string>& args) {
		// POSIX-like behaviour.
		if (!TCLAP::Arg::ignoreRest()) {
			const std::string& s = args[*i];
			if (s.length() == 0 || s[0] == TCLAP::Arg::flagStartChar()) {
				return false;
			}
		}

		return TCLAP::UnlabeledMultiArg<std::string>::processArg(i, args);
	}

	virtual std::string shortID(const std::string& val = "") const {
		std::string original = TCLAP::UnlabeledMultiArg<std::string>::shortID(val);

		bool required = isRequired() || visual_required_override;

		return std::string(1, option_brackets[required][0])
			+ original.substr(1, original.length() - 2 - 4)
			+ std::string(1, option_brackets[required][1])
			+ "...";
	}

	MultiArgumentOption(const std::string& name, const std::string& desc, bool req, const std::string& typeDesc) :
		TCLAP::UnlabeledMultiArg<std::string>(name, desc, req, typeDesc) {}
};


template<typename T>
class ZToolsValueArg : public TCLAP::ValueArg<T> {
	public:
	ZToolsValueArg(const std::string& flag,
		const std::string& name,
		const std::string& desc,
		bool req,
		T value,
		const std::string& typeDesc) : ValueArg<T>(flag, name, desc, req, value, typeDesc) {}

	ZToolsValueArg(const std::string& flag,
		const std::string& name,
		const std::string& desc,
		bool req,
		T value,
		TCLAP::Constraint<T>* constraint) : ValueArg<T>(flag, name, desc, req, value, constraint) {}

	virtual inline std::string longID(const::std::string& val = "") const {
		using namespace std;
		using namespace TCLAP;
		(void) val;

		const std::string& valueId = this->_typeDesc;

		string id;

		if (this->getFlag().length() > 0) {
			id += Arg::flagStartString() + this->getFlag() + ",  ";
		}

		id += Arg::nameStartString() + this->getName();

		if (this->isValueRequired()) {
			id += string(2, Arg::delimiter()) + std::string(1, option_brackets[1][0]) + valueId + std::string(1, option_brackets[1][1]);
		}

		return id;
	}
};


template<typename T>
class ZToolsMultiArg : public TCLAP::MultiArg<T> {
	public:
	ZToolsMultiArg(const std::string& flag,
		const std::string& name,
		const std::string& desc,
		bool req,
		const std::string& typeDesc) : TCLAP::MultiArg<T>(flag, name, desc, req, typeDesc) {}

	ZToolsMultiArg(const std::string& flag,
		const std::string& name,
		const std::string& desc,
		bool req,
		TCLAP::Constraint<T>* constraint) : TCLAP::MultiArg<T>(flag, name, desc, req, constraint) {}

	virtual inline std::string longID(const::std::string& val) const {
		using namespace std;
		using namespace TCLAP;
		(void) val;

		const std::string& valueId = this->_typeDesc;

		string id;

		if (this->getFlag().length() > 0) {
			id += Arg::flagStartString() + this->getFlag() + ",  ";
		}

		id += Arg::nameStartString() + this->getName();

		if (this->isValueRequired()) {
			id += string(2, Arg::delimiter()) + std::string(1, option_brackets[1][0]) + valueId + std::string(1, option_brackets[1][1]);
		}

		id += "  (accepted multiple times)";

		return id;
	}
};

template<typename T>
class StrOptTranslator {
	public:
	// List of options
	std::vector<std::string> opts;

	// Mapping of option name to internal value
	std::map<std::string, T> opt_map;

	std::string default_opt;

	void push_opt(const std::string& name, T val) {
		opts.push_back(name);
		opt_map.insert(make_pair(name, val));
	}

	T translate(const std::string& name) const {
		typename std::map<std::string, T>::const_iterator it = opt_map.find(name);
		if (it == opt_map.end()) {
			throw(Error("Invalid option " + name));
		}
		return it->second;
	}

	std::string inverseTranslate(const T val) const {
		typename std::map<std::string, T>::const_iterator it;
		for (it = opt_map.begin(); it != opt_map.end(); ++it) {
			if (it->second == val) {
				return it->first;
			}
		}
		return "";
	}

	T translate(TCLAP::ValueArg<std::string>& a) const {
		return translate(a.getValue());
	}
};

// Message appended to the bottom of the (long) usage statement.
static const std::string usage_message =
R"(If input-file is a TEX image, it is converted to a PNG.
Otherwise, it is converted to TEX. The format of input-file is
inferred from its binary contents (its magic number).

If output-dir is not given, then it is taken as the directory containing input-path
(thus placing it in the current directory). If output-path is a directory,
this same rule applies, but the resulting file is placed in it instead.)";

static const std::string license =
R"(ZTools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZTools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZTools.  If not, see <https://www.gnu.org/licenses/>.)";

static const std::string TEX_BOTH = "Options for TEX input and output";
static const std::string FROM_TEX = "Options for TEX input";
static const std::string TO_TEX = "Options for TEX output";
static const std::string ATLAS_BOTH = "Options for atlas input and output";
static const std::string FROM_ATLAS = "Options for atlas input";
static const std::string TO_ATLAS = "Options for atlas output";
static const std::string SYMBOL_RENAME = "Options for renaming strings in a build.bin+anim.bin";

class tex;

class parsed_command_line {
	public:
	tex::compression_types compression_type;
	MagickCore::FilterType resize_type;
	tex::texture_types texture_type;

	bool no_mipmaps;
	bool no_premultiply;
	bool no_atlas;

	bool pre_caves;

	float atlas_pixel_offset;
	uint16_t atlas_width;
	uint16_t atlas_height;

	bool print_string;
	std::vector<std::string> string_renames;

	int verbosity;

	std::string input_path;
	std::string secondary_input_path;
	std::string output_path;

	parsed_command_line(int argc, char **& argv) {
		try {
			ZToolsOutput output{license};
			TCLAP::CmdLine cmd{usage_message, ' ', PACKAGE_VERSION};
			cmd.setOutput(&output);

			std::list<TCLAP::Arg*> args;

			std::list<TCLAP::Arg*>& raw_args = cmd.getArgList();
			raw_args.push_back(raw_args.front());
			raw_args.pop_front();

			output.addCategory(TEX_BOTH);
			output.addCategory(FROM_TEX);
			output.addCategory(TO_TEX);
			output.addCategory(ATLAS_BOTH);
			output.addCategory(FROM_ATLAS);
			output.addCategory(TO_ATLAS);
			output.addCategory(SYMBOL_RENAME);

			MultiArgumentOption input_arg("INPUT-PATH", "Input path.", true, "INPUT-PATH");
			cmd.add(input_arg);

			//ArgumentOption secondary_input_arg("SECONDARY-INPUT-FILE", "Secondary input file.", false, "SECONDARY-INPUT-FILE");
			//cmd.add(secondary_input_arg);

			ArgumentOption output_arg("OUTPUT-DIR", "Output directory.", false, "OUTPUT-DIR");
			cmd.add(output_arg);

			StrOptTranslator<tex::compression_types> compression_opts;
			compression_opts.push_opt("DXT1", tex::compression_types::DXT1);
			compression_opts.push_opt("DXT3", tex::compression_types::DXT3);
			compression_opts.push_opt("DXT5", tex::compression_types::DXT5);
			compression_opts.push_opt("RGBA", tex::compression_types::RGBA);
			compression_opts.push_opt("RGB", tex::compression_types::RGB);
			compression_opts.push_opt("BC1", tex::compression_types::DXT1);
			compression_opts.push_opt("BC2", tex::compression_types::DXT3);
			compression_opts.push_opt("BC3", tex::compression_types::DXT5);
			compression_opts.default_opt = "DXT5";

			TCLAP::ValuesConstraint<std::string> allowed_compressions(compression_opts.opts);

			ZToolsValueArg<std::string> compression("c", "compression", "Compression type for TEX creation. Defaults to " + compression_opts.default_opt + ".", false, compression_opts.default_opt, &allowed_compressions);
			args.push_back(&compression);
			output.setArgCategory(compression, TO_TEX);

			StrOptTranslator<MagickCore::FilterType> resize_opts;
			resize_opts.push_opt("lanczos", Magick::LanczosFilter);
			resize_opts.push_opt("lanczos2", Magick::Lanczos2Filter);
			resize_opts.push_opt("hermite", Magick::HermiteFilter);
			resize_opts.push_opt("hann", Magick::HannFilter);
			resize_opts.push_opt("blackman", Magick::BlackmanFilter);
			resize_opts.push_opt("gaussian", Magick::GaussianFilter);
			resize_opts.push_opt("cubic", Magick::CubicFilter);
			resize_opts.push_opt("catrom", Magick::CatromFilter);
			resize_opts.push_opt("mitchell", Magick::MitchellFilter);
			resize_opts.push_opt("jinc", Magick::JincFilter);
			resize_opts.push_opt("sinc", Magick::SincFilter);
			resize_opts.push_opt("kaiser", Magick::KaiserFilter);
			resize_opts.push_opt("welch", Magick::WelchFilter);
			resize_opts.push_opt("parzen", Magick::ParzenFilter);
			resize_opts.push_opt("bohman", Magick::BohmanFilter);
			resize_opts.push_opt("bartlett", Magick::BartlettFilter);
			resize_opts.push_opt("lagrange", Magick::LagrangeFilter);
			resize_opts.push_opt("robidoux", Magick::RobidouxFilter);
			resize_opts.push_opt("cosine", Magick::CosineFilter);
			resize_opts.push_opt("spline", Magick::SplineFilter);
			resize_opts.push_opt("sentinel", Magick::SentinelFilter);
			resize_opts.push_opt("box", Magick::BoxFilter);
			resize_opts.default_opt = "lanczos";

			TCLAP::ValuesConstraint<std::string> allowed_resizes(resize_opts.opts);

			ZToolsValueArg<std::string> resize("r", "resize", "Resizing filter used for mipmap generation. Defaults to " + resize_opts.default_opt + ".", false, resize_opts.default_opt, &allowed_resizes);
			args.push_back(&resize);
			output.setArgCategory(resize, TO_TEX);

			StrOptTranslator<tex::texture_types> texture_opts;
			texture_opts.push_opt("1D", tex::texture_types::_1D);
			texture_opts.push_opt("2D", tex::texture_types::_2D);
			texture_opts.push_opt("3D", tex::texture_types::_3D);
			texture_opts.push_opt("CubeMap", tex::texture_types::CUBEMAP);
			texture_opts.default_opt = "2D";

			TCLAP::ValuesConstraint<std::string> allowed_textures(texture_opts.opts);

			ZToolsValueArg<std::string> texture("t", "texture", "Target texture type. Defaults to " + texture_opts.default_opt + ".", false, texture_opts.default_opt, &allowed_textures);
			args.push_back(&texture);
			output.setArgCategory(texture, TO_TEX);

			TCLAP::SwitchArg no_premultiply_flag("", "no-premultiply", "Don't premultiply alpha.");
			args.push_back(&no_premultiply_flag);
			output.setArgCategory(no_premultiply_flag, TO_TEX);

			TCLAP::SwitchArg no_mipmaps_flag("", "no-mipmaps", "Don't generate mipmaps.");
			args.push_back(&no_mipmaps_flag);
			output.setArgCategory(no_mipmaps_flag, TO_TEX);

			TCLAP::SwitchArg no_atlas_flag("", "no-atlas", "Don't generate an atlas.");
			args.push_back(&no_atlas_flag);
			output.setArgCategory(no_atlas_flag, TO_TEX);

			TCLAP::SwitchArg do_pre_caves("", "pre-caves", "Try loading the file with the pre-caves format.");
			args.push_back(&do_pre_caves);
			output.setArgCategory(do_pre_caves, FROM_TEX);

			ZToolsValueArg<float> pixel_offset("", "atlas-pixel-offset", "Pixel offset for each image in the atlas. Defaults to 0.5.", false, 0.5f, "Value in pixels to offset");
			args.push_back(&pixel_offset);
			output.setArgCategory(pixel_offset, ATLAS_BOTH);

			ZToolsValueArg<uint16_t> max_atlas_width("", "atlas-width", "Maximum width of the atlas. Defaults and capped to 2048.", false, 2048, "Atlas width");
			args.push_back(&max_atlas_width);
			output.setArgCategory(max_atlas_width, TO_ATLAS);

			ZToolsValueArg<uint16_t> max_atlas_height("", "atlas-height", "Maximum height of the atlas. Defaults and capped to 2048.", false, 2048, "Atlas height");
			args.push_back(&max_atlas_height);
			output.setArgCategory(max_atlas_height, TO_ATLAS);

			ZToolsMultiArg<std::string> string_rename("s", "string-rename", "Rename a string.", false, "string:renameto");
			args.push_back(&string_rename);
			output.setArgCategory(string_rename, SYMBOL_RENAME);

			TCLAP::SwitchArg print_strings("", "print-strings", "Print strings for swapping.");
			args.push_back(&print_strings);
			output.setArgCategory(print_strings, SYMBOL_RENAME);

			TCLAP::MultiSwitchArg verbosity_flag("v", "verbose", "Increases output verbosity.");
			args.push_back(&verbosity_flag);

			TCLAP::SwitchArg quiet_flag("q", "quiet", "Disables text output. Overrides the verbosity value.");
			args.push_back(&quiet_flag);

			for (auto arg = args.rbegin(); arg != args.rend(); ++arg) {
				cmd.add(*arg);
			}

			cmd.parse(argc, argv);

			compression_type = compression_opts.translate(compression);
			resize_type = resize_opts.translate(resize);
			texture_type = texture_opts.translate(texture);

			no_premultiply = no_premultiply_flag.getValue();
			no_mipmaps = no_mipmaps_flag.getValue();
			no_atlas = no_atlas_flag.getValue();

			pre_caves = do_pre_caves.getValue();

			atlas_pixel_offset = pixel_offset.getValue();
			atlas_width = std::min<uint16_t>(2048, max_atlas_width.getValue());
			atlas_height = std::min<uint16_t>(2048, max_atlas_height.getValue());

			string_renames = string_rename.getValue();
			print_string = print_strings.getValue();

			if (quiet_flag.getValue()) {
				verbosity = -1;
			} else {
				verbosity = verbosity_flag.getValue();
			}

			std::vector<std::string> input_args = input_arg.getValue();
			if (input_args.size() == 3) {
				output_path = input_args.back();
				input_args.pop_back();
				secondary_input_path = input_args.back();
				input_args.pop_back();
				input_path = input_args.back();
				input_args.pop_back();
			} else if (input_args.size() == 2) {
				secondary_input_path = input_args.back();
				input_args.pop_back();
				input_path = input_args.back();
				input_args.pop_back();
			} else if (input_args.size() == 1) {
				input_path = input_args.back();
				input_args.pop_back();
			}
		} catch (TCLAP::ArgException &e) {// catch any exceptions
			std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
		} catch (std::exception& e) {
			std::cerr << "error: " << e.what() << std::endl;
			exit(1);
		}
	}
	private:

};