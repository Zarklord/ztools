# ztools
A cross-platform modding tool for the game [Don't Starve Together](https://www.klei.com/games/dont-starve-together), by [Klei Entertainment](http://kleientertainment.com/).

**IMPORTANT**: In what follows, a code block starting with a `$` indicates something that should be typed in a terminal (cmd.exe, for Windows). The `$` and the space following it should not be typed.

### Basic usage and examples
ztools converts bidirectionally between Klei's TEX format(KTEX) and most image formats. if the first argument is a KTEX file, it will be converted to a png, if the first argument is an image file, it will be converted to a KTEX file, if the first argument is a folder, ztools will atempt to create an atlas with all the image files in that folder, if the first arguement is an xml ztools will attempt to locate a KTEX file under the xml path with the extension replaced by ".tex", or you can manually specify the KTEX file with a second argument.

to convert `atlas-0.tex` to `atlas-0.png`:
```
$ ztools atlas-0.tex
```
to do the same, placing it in some_folder:
```
$ ztools atlas-0.tex some_folder
```
to convert `atlas_images`(a folder full of images to be atlased) to an atlas_images.xml and atlas_images.tex:
```
$ ztools atlas_images
```
to do the same, placing it in some_folder:
```
$ ztools atlas_images some_folder
```

###Full usage
To get full usage info, enter:
```
$ ztools --help
```

## INSTALLATION FROM SOURCE
First, install [CMake](http://www.cmake.org) this project requires atleast version 3.9 of cmake, and [ImageMagick](http://www.imagemagick.org) install the [32 Bit(x86) Q16 DLL version](https://imagemagick.org/archive/binaries/ImageMagick-7.1.0-39-Q16-x86-dll.exe)  and a native building solution (such as Visual Studio for Windows, XCode for Mac and gcc/GNU Make for Linux; in the Linux case, you probably have them already).

### Linux and Mac 
Enter ztools directory with a terminal and type
```
$ mkdir build && cd build
$ cmake ../ && make
```
provided there are no errors, ztools should be placed in build/Release directory

### Windows
Enter ztools directory with a terminal and type
```
$ mkdir build && cd build
$ cmake -A Win32 ../
```
open the build/ztools.sln file, right click the 'ztools' solution on the left pane, click on "Configuration Manager..." and make sure to select "Release" as the active solution configuration. Then build the solution (which may be done by pressing "F7").

## LICENSE
See LICENSE